var itemsListAvailable = false;
var ajaxOptions = {
  url : '/api/inventory',
  dataType: 'json',
  success: onSearchResponse,
  error: onSearchError,
};
var cachedItemList = {};
var transactionApp = new Vue({
  el: '#transactions-app',
  data : {
    inventoryItems: [],
    itemsAvailable: false,
    purchasedItems: [],
    subTotal: 0,
    vat: 5,
    total: 0,
  }
});

$('#transaction').addClass('selected');
$('.clear-form-btn').click(function() {
  window.location.href = window.location;
});

//Transaction receipts functions
$('#process').click(function() {
  // $('#transaction-modal').addClass('show');
  showModal('#transaction-modal');
});

$('#receipt-close').click(function(){
  // $('#transaction-modal').removeClass('show');
  closeModal('#transaction-modal');
});

$('#add-item-modal').on('click', '.add-to-cart-btn', function (el) {
  console.log('Adding item to cart');
  console.log(el.currentTarget.id);
  var matchFound = false;
  if(transactionApp.purchasedItems.length > 0) {
    transactionApp.subTotal = 0;
    transactionApp.purchasedItems.map(function(item) {
      if(item.id == el.currentTarget.id) {
        item.qty_available--;
        item.qty_purchased++;
        item.price = item.qty_purchased * item.unit_price;
        matchFound = true;
      }

      transactionApp.subTotal += item.price;
    });
  }

  if(!matchFound) {
    transactionApp.inventoryItems.map(function(item) {
      if(item.id == el.currentTarget.id) {
        item.qty_available--;
        item.qty_purchased = 1
        item.price = item.unit_price;
        transactionApp.purchasedItems.push(item);
        return true;
      }
    });
  }
  
  transactionApp.total = transactionApp.subTotal + transactionApp.vat;
});

//Item search related functions
$('#add-item').click(function() {
  // console.log('Add item clicked');
  // $('#search-modal').addClass('show');
  showModal('#add-item-modal');
  if(itemsListAvailable == false)
    $.ajax(ajaxOptions);
  else {
    onSearchResponse(cachedItemList);
  }
});

$('#search-close').click(function(){
  // $('#search-modal').removeClass('show');
  closeModal('#add-item-modal');
});



function onSearchResponse(response) {
  itemsListAvailable = true;
  if(response == cachedItemList)
    console.log('Showing cached results');
  else {
    console.log('Showing network response: ' + response);
    cachedItemList = response;
  }

  transactionApp.inventoryItems = cachedItemList;
  transactionApp.itemsAvailable = cachedItemList.length > 0;
  // console.log(cachedItemList);
}

function onSearchError(req, status, err) {
    console.log('Something went wrong', status, err);
}
