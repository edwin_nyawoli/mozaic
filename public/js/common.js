//Shows a modal dialog with the given name
function showModal(modal) {
  $(modal).addClass('show');
}

//Closes a modal dialog with the given name
function closeModal(modal) {
  $(modal).removeClass('show');
}
