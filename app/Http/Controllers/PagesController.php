<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{

    public function __construct()
    {
      // $this->middleware('guest');
    }

    //
    public function index()
    {
      $title = 'Home';
      return view('app', compact('title'));
    }
}
