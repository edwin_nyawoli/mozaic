<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;

class TransactionController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    
    //
    public function index(Request $request)
    {
      $transactions = Transaction::get();

      if($request->wantsJson())
        return json_encode($transactions);
      else
        return $transactions;
    }

    public function create()
    {
      return view('transaction.create');
    }

    public function store(Request $request)
    {
      return json_encode($request->all());
    }
}
