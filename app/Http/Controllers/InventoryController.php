<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
// use Request;

class InventoryController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:web,api');
    }

    public function index(Request $request)
    {
      $inventory = Inventory::get();
      if($request->wantsJson())
        return json_encode($inventory);
      else
        return view('inventory.inventory', compact('inventory'));
    }

    public function store(Request $request)
    {
      Inventory::create($request->all());
      return redirect('/inventory');
    }

    public function destroy($id)
    {
      $item = Inventory::find($id);
      $item->delete();
      return redirect('/inventory');
    }
}
