# Mozaic
Mozaic is a simple Web based Point-Of-Sale (POS) System.
It provides a clean user interface through which you can perform POS related activities.

## Features
* Transaction Processing - Streamlined transaction flow; from adding items to receiving payment.
* Inventory Management - Easily view, add, remove or update inventory items with details.
* Report Generation - Get detailed reports on all recorded activities.
* User Access Control - Control which actions or information are available to users of the system.
