@extends('app')

@section('content')
  <h1>No Reports have been generated yet.</h1>
@stop

@section('scripts')
  <script type="text/javascript">
    $('#reports').addClass('selected');
  </script>
@stop
