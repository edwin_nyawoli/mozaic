<div class="search-bar form-inline">
  <form class="" action="/search" method="post">
    <input style="width:85%;" type="text"  name="search-field" class="search-field form-control" value="" placeholder="Type search parameters here">
    <input type="button" name="search" value="Search" class="btn btn-primary">
  </form>
</div>
