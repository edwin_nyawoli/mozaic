<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css" media="screen" title="no title">
    <link rel="stylesheet" href="/css/master.css" media="screen" title="no title">
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/vue.min.js"></script>
    <script src="/js/common.js"></script>
    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            @if(Auth::check())
            'Authorization': 'Bearer ' + '{{ Auth::user()->api_token }}'
            @endif
        }
      });
    </script>
    <!-- <script src="https://unpkg.com/vue/dist/vue.js"></script> -->
    <title>Mozaic POS</title>
    @yield('styles')
    <style media="screen">
      .btn-primary {
        /*background-color: #373560;*/
      }
    </style>
  </head>
  <body>
    <div class="container-full">
      <div class="side-nav">
        <div class="user-details">

        </div>
        <span id="title">Mozaic</span>
        <ul class="nav-list">
          <li>
            <a class="nav-item" id="transaction" href="/transactions/create">Transactions</a>
          </li>
          <li>
            <a class="nav-item" id="inventory" href="/inventory">Inventory</a>
          </li>
          <li>
            <a class="nav-item" id="reports" href="/reports">Reports</a>
          </li>
          <li>
            <a class="nav-item" id="users" href="/users">Users</a>
          </li>
        </ul>
      </div><div class="content">
        <div class="control-bar">
          @if(Auth::check())
            <form class="" action="/logout" method="POST">
              {{ csrf_field() }}
              <button type="submit" name="button" class="btn btn-primary" style="float:right">Logout</button>
            </form>
          @endif

          @yield('control-bar')
        </div>
        <div class="container">
          @yield('content')
        </div>
      </div>
    </div>
    @yield('scripts')
  </body>
</html>
