<div class="">
  <table class="table table-hover">
    <tr>
      <th>
        Name
      </th>
      <th>
        Qty Available
      </th>
      <th>
        Actions
      </th>
    </tr>
    @foreach($inventory as $item)
    <tr>
      <td>
        {{ $item->name }}
      </td>
      <td>
        {{ $item->qty_available }}
      </td>
      <td>
        <form class="form-inline" action="/inventory/{{ $item->id }}" method="POST">
          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="submit" name="update-btn" class="btn btn-primary" value="Update">
        </form>
        <form class="form-inline" action="/inventory/{{ $item->id }}" method="POST">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="submit" name="remove-btn" class="btn btn-danger" value="Remove">
        </form>
      </td>
    </tr>
    @endforeach
  </table>
</div>
