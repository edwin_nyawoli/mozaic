<div id="add-item-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <button type="button" id="modal-close" class="close " aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3>New Item Details</h3>
    <form class="" action="inventory" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
        <label for="">Name</label>
        <input type="text" name="name" value="" class="form-control" required="required">
      </div>
      <div class="form-group">
        <label for="">Description</label>
        <input type="text" name="description" value="" class="form-control" required="required">
      </div>
      <div class="form-group">
        <label for="">Quantity Available</label>
        <input type="text" name="qty_available" value="" class="form-control" required="required">
      </div>
      <div class="form-group">
        <label for="">Unit Price</label>
        <input type="text" name="unit_price" value="" class="form-control" required="required">
      </div>
      <div class="form-group">
        <input type="reset" name="reset" class="btn btn-warning" value="Clear">
        <input type="submit" name="submit" class="btn btn-primary" value="Add Item">
      </div>
    </form>
  </div>
</div>
