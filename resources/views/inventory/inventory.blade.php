@extends('app')
@section('styles')
  <style media="screen">
    table {
      width: 100%;
    }
  </style>
@stop

@section('content')
  @if($size = count($inventory))
    <h1>Inventory</h1>
    <span>{{ $size }} item(s) currently in inventory.</span>
    <button type="button" id="add-item" name="button" class="btn btn-primary">Add New Item</button>
    @include('inventory.list')
  @else
    <h3>There are no items in the inventory</h3>
    <button type="button" id="add-item" name="button" class="btn btn-primary">Add New Item</button>
  @endif
  @include('inventory.add-item')
@stop

@section('scripts')
<script type="text/javascript">
  $('#inventory').addClass('selected');
</script>
  <script src="/js/inventory.js">
  </script>
@stop
