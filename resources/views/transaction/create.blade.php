@extends('app')
@section('styles')
<style media="screen">

  table {
    width: 100%;
  }

</style>
@stop
@section('content')
    <form id="transactions-app" class="" action="/transactions" method="post">
      <div class="">
        <h1>New Transaction</h1>
        <div class="form-inline">
          <label for="">Transaction No#</label>
          <input type="text" name="transaction_number" value="00{{ Auth::id()}}" readonly="readonly" class="form-control">
        </div>
        <div class="form-inline">
          <label for="">Teller Name</label>
          <input type="text" name="teller_name" value="{{ Auth::user()->name}}" readonly="readonly" class="form-control">
        </div>
        <div class="form-inline">
          <label for="customer_name">Customer Name</label>
          <input type="text" name="customer_name" class="form-control" value="">
        </div>
        <hr />
        <h3>Items being purchased</h3>

        <div class="item-rows">
          <table class="table">
            <tr>
              <th name="name">Name</th>
              <th name="qty">Qty Purchased</th>
              <th name="price">Price</th>
            </tr>
          </table>
          <table id="item-table" class="table table-hover">

            <tr v-for="item in purchasedItems">
              <?php
                echo "
                <td>
                  <input type='hidden' v-bind:name='item.id' v-bind:value='item.qty_purchased'>
                  {{ item.name }}
                </td>
                <td>
                  {{ item.qty_purchased }}
                </td>
                <td>
                  GHC{{ item.price }}
                </td>
                "
              ?>
            </tr>
          </table>
          <input type="button" id="add-item" class="btn btn-primary" value="Add an Item">
          <hr/>
          <table>
            <tr>
              <td>SubTotal:</td>
              <td name="subtotal">
                <span class="currency">GHC</span>
                <?php echo "{{ subTotal }}" ?>
              </td>
            </tr>
            <tr>
              <td>VAT:</td>
              <td name="vat">
                <span class="currency">GHC</span>
                <?php echo "{{ vat }}" ?>
              </td>
            </tr>
            <tr>
              <td>Total:</td>
              <td name="total">
                <span class="currency">GHC</span>
                <?php echo "{{ total }}" ?>
              </td>
            </tr>
          </table>
          <hr />
        </div>
        <input type="button" class="btn btn-warning clear-form-btn" value="Clear Transaction">
        <input type="button" id="process" class="btn btn-primary" value="Process Transaction">
      </div>
    @include('transaction.receipt')
    @include('transaction.add-item-modal')
  </form>
@stop

@section('scripts')
  <script src="/js/transaction.js">
  </script>
@stop
