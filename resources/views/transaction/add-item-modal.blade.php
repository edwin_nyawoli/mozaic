<!-- The Search Modal -->
<div id="add-item-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <button type="button" id="search-close" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3>Add Item to Transaction</h3>
    <span>Search or select from below an item to add</span>
    <!-- <span  class="modal-close">x</span> -->
    @include('search')
    <hr>
    <button type="button" name="button" class="add-to-cart-btn hidden"></button>
    <table id="inventory-items" v-if="itemsAvailable">
      <tr>
        <td>
          Name
        </td>
        <td>
          Qty Available
        </td>
        <td>
          Action
        </td>
      </tr>

      <tr v-for="item in inventoryItems">
        <?php
          echo "
          <td>
            {{ item.name }}
          </td>
          <td>
            {{ item.qty_available }}
          </td>
          <td>
            <button v-bind:id='item.id' type='button' name='button' class='btn btn-primary add-to-cart-btn'>Add +1</button>
          </td>
          "
         ?>
      </tr>
    </table>
    <!-- <h3 v-else="itemsAvailable">There are no inventory items available.</h3> -->
  </div>
</div>
