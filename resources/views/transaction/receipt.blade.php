<!-- The Process Transaction Modal -->
<div id="transaction-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <!-- <span  class="modal-close">x</span> -->
    <button type="button" id="receipt-close" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3>Confirm Transaction</h3>
    <p>
      Are the details of this transaction valid?
    </p>
    <button type="button" name="button" class="btn btn-warning clear-form-btn">No, Cancel Transaction</button>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit" name="cplt-trsc" class="btn btn-primary" value="Yes, Complete Transaction"/>
  </div>
</div>
